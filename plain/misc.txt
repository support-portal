Q. How is Tor different from a VPN?
A. 

Q. Can I use Tor with bittorrent?
A. Using Tor with bittorrent is not recommended or supported. For further details,
please see our blog post on the subject at https://blog.torproject.org/blog/bittorrent-over-tor-isnt-good-idea. 

Q. Why did you infect my computer with malware? (CryptoLocker)
A. You have been infected with malware. The Tor Project did not create this malware. 

The Tor Project writes software that is used every day for a wide variety of purposes 
by the military, journalists, law enforcement officers, activists, and many others
[https://www.torproject.org/about/torusers.html.en].  Unfortunately, the protection that our software
can provide to these groups of people can also be abused by criminals and malware authors.

The Tor Project does not endorse, support or condone the use of our software in this way. However,
we are not able to assist directly with these cases as we did not write the malware that you
have been infected with.

Q. Does Tor keep logs?
A. Tor doesn't keep any longs that could identify a particular user. We do take some measurements of how the network functions, which you can check out here: metrics.torproject.org.

Q. Does Tor Project offer xmpp/email/some other privacy-enhancing technologies?
A. No, we don't provide any online services. A list of all of our software projects can be found here: https://www.torproject.org/projects/projects.html.en

Q. How do I add Tor to my product?
A.

Q. Can I use the Tor logo in my product/movie/book/etc?
A. You can read all about that here: https://www.torproject.org/docs/trademark-faq.html

Q. Will Tor show me who is tracking me?
A.

Q. Will Tor Project help me track this Tor user who is doing something I
don't like?
A.

Q. How do I volunteer with Tor Project?
A.

Q. How do I reach the Tor Project press person?
A.

Q. How do I report a bug or some other issue?
A.

Q. How do I report something wrong on the website?
A.

Q. I'm having a problem updating/using Vidalia.
A. Vidalia is no longer maintained or supported. A large portion of the features Vidalia offered have now been integrated into the Tor Browser itself. 

Q. Why can't I use Vidalia anymore?
A. Vidalia is no longer maintained or supported. A large portion of the features Vidalia offered have now been integrated into the Tor Browser itself. 

Q. What is a .onion?
A. 

Q. Can I use a VPN with Tor?
A.

Q. Will you host my email/website?
A.

Q. Where can I find RPM packages?
A.

Q. Where can I find Debian / Ubuntu packages?
A. 

Q. Does the Tor Project offer hosting?
A. No, the Tor Project does not offer hosting services.

Q. Do any VoIP applications work with Tor?
A.

Q. Can I change the number of hops Tor uses?
A. Right now the path length is hard-coded at 3 plus the number of nodes in your path that are sensitive. That is, in normal cases it's 3, but for example if you're accessing an onion service or a ".exit" address it could be 4.

We don't want to encourage people to use paths longer than this as it increases load on the network without (as far as we can tell) providing any more security. Also, using paths longer than 3 could harm anonymity, first because it makes "denial of security"[http://freehaven.net/anonbib/#ccs07-doa] attacks easier, and second because it could act as an identifier if only a small number of users have the same path length as you.

